class Boid{
	//vx=5;
	//vy=5;

	constructor(x,y,ctx){
		this.x=x;
		this.y=y;
		this.ctx=ctx;
		this.vx=5;
		this.vy=5;
		ctx.fillStyle = 'black';
	}
	
	

	drawBoid()
	{
		//this.ctx.clearRect(0,0,canvas.width,canvas.height);
		this.ctx.beginPath();
		//this.ctx.moveTo(50,50);
		//this.ctx.lineTo(85,85);
		//this.ctx.lineTo(52,85);
		//this.ctx.lineTo(82,87);
		////this.ctx.closePath();		
		this.ctx.moveTo(this.x, this.y);
        this.ctx.lineTo(this.x + (-30), this.y + (-25));
        this.ctx.lineTo(this.x + (-30), this.y -12);
		this.ctx.lineTo(this.x + (-45), this.y -12);
		this.ctx.fill();
		
		

		
	}

	moveBoid()
	{
		
		let angle=Math.atan2(this.vy,this.vx);
		this.ctx.save();
		this.ctx.translate(this.x,this.y);
		this.ctx.rotate(angle);
		this.ctx.translate(-this.x,-this.y);
		this.drawBoid();
		this.ctx.restore();
		this.x+=this.vx;
		this.y+=this.vy;
		if(this.x>canvas.width)
		{
			this.x=0;
		}
		if(this.y>canvas.height)
		{
			this.y=0;
		}
		if(this.y<0)
		{
			this.y=canvas.height;
		}
		if(this.x<0)
		{
			this.x=canvas.width;
		}
	}



}