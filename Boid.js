class Boid 
{
    drawRadius = false;
    positionHistory = [];
    color;

    constructor(x, y, canvas, ctx) 
	{
        this.position = new Victor(x, y);
        this.speed = new Victor( ( Math.random() - 0.5 ) * 2, ( Math.random() - 0.5 ) * 2);
        this.acceleration =  new Victor(0,0);
        
        this.canvas = canvas;
        /**@type {CanvasRenderingContext2D} */
        this.ctx = ctx;
    }

    Draw() 
	{
        this.ctx.fillStyle = this.color;
		this.ctx.beginPath();
		this.ctx.lineTo(this.position.x + (10), this.position.y + (0));
        this.ctx.lineTo(this.position.x + (-4), this.position.y + (-4));
        this.ctx.lineTo(this.position.x + (-4), this.position.y + (4));
        this.ctx.lineTo(this.position.x + (10), this.position.y + (0));
		this.ctx.fill();
        /*this.ctx.lineTo(this.position.x + (15), this.position.y + (0));
        this.ctx.lineTo(this.position.x + (0), this.position.y + (-5));
        this.ctx.lineTo(this.position.x + (-5), this.position.y + (-7));
        this.ctx.lineTo(this.position.x + (-10), this.position.y + (-5));
        this.ctx.lineTo(this.position.x + (-15), this.position.y + (-3));
	    this.ctx.lineTo(this.position.x + (-15), this.position.y + (3));
        this.ctx.lineTo(this.position.x + (-10), this.position.y + (5));
	    this.ctx.lineTo(this.position.x + (-5), this.position.y + (7));
        this.ctx.lineTo(this.position.x + (0), this.position.y + (5));
        this.ctx.lineTo(this.position.x + (15), this.position.y + (0));
        this.ctx.fill();
		*/
        //Disegno il raggio di ricerca
        if(this.drawRadius)
		{
            this.ctx.arc(this.x, this.y, searchRadius, 0, Math.PI*2);
            this.ctx.stroke;
        }
            
    }

    Move() 
	{
        //Calcolo l'angolo
        let corner = this.speed.angle();
		let angle = this.speed.angleDeg();
		if(angle<0)
		{
			angle = angle+360;
		}
		this.color = '#00' + Math.floor(20+(235/360) * angle).toString(16) + 'ff';
		//console.log(angle);
        //Salvo la posizione iniziale
        this.ctx.save();
        //Mi sposto all'inizio del triangolo
        this.ctx.translate(this.position.x, this.position.y);
        //Ruoto il canvas
        this.ctx.rotate(corner);
        //Sposto il canvas all'origine
        this.ctx.translate(-this.position.x, -this.position.y);
        // Disegno il triangolo
        this.Draw();
        //Resetto la rotazione
        this.ctx.restore();

        //Prima di spostare il boid salvo l'ultima posizione nell'array
        let tmp;
        this.positionHistory.unshift(tmp = 
		{
            x: this.position.x,
            y: this.position.y,
        });
		
        //Trim a massimo nScia posizioni
        this.positionHistory = this.positionHistory.slice(0, nTrail);

        //Sposto il boid
        this.position.x += this.speed.x;
        this.position.y += this.speed.y;

        //Bordo basso
        if(this.position.y > this.canvas.height) 
		{
            this.position.y = 0;
        }

        //Bordo destro
        if(this.position.x > this.canvas.width) 
		{
            this.position.x = 0;
        }

        //Bordo sinistro
        if(this.position.x < 0) 
		{
            this.position.x = this.canvas.width;
        }

        //Bordo alto
        if(this.position.y < 0) 
		{
            this.position.y = this.canvas.height;
        }
    }

    DrawTrail() 
	{
        //Variabile per segnare l'ulitma volta che il disegno è stato fermato
        let lastDraw = 0;
        //Disegno la scia
        for (let i = 0; i < this.positionHistory.length; i++) 
		{
            //Per evitare che la righa salti in giro, controllo se la prossima posizione è a più di un terzo della grandezza del canvas
            //Calcolo la distanza
            let distance;
            try 
			{
                distance = Math.sqrt((this.positionHistory[i + 1].x - this.positionHistory[i].x) ** 2 +
                    (this.positionHistory[i + 1].y - this.positionHistory[i].y) ** 2);
            }catch 
			{
                distance = 0;
            }

            //Controllo se questa è o la prima volta che deve essere iniziato il disegno, oppure un'altra dopo che il disegno è stato ultimato
            //Ma è ancora incompleto
            if (i == lastDraw) 
			{
                //Anche qui controllo e salto il disegno se la distanza è troppa
                if (distance >= canvas.width / 3 || distance >= canvas.height / 3) 
				{
                    lastDraw = i + 1;
                }else 
				{
                    ctx.beginPath();
                    ctx.moveTo(this.positionHistory[i].x, this.positionHistory[i].y);
                }
            }else 
			{

                //Controllo se la distanza è maggiore di un terzo del canvas
                if(distance >= canvas.width / 3 || distance >= canvas.height / 3) 
				{
                    //Stroke e rinizio settando ultimaposizione a i + 1
                    ctx.stroke();
                    lastDraw = i + 1;
                }else 
				{
                    ctx.lineTo(this.positionHistory[i].x, this.positionHistory[i].y);
                }
            }
        }
        ctx.strokeStyle = this.color;
        ctx.stroke();
    }

    ApplyForce(force, maxSpeedX, maxSpeedY)
    {
        this.speed.add(force);

        if (this.speed.x > maxSpeedX)
		{
            this.speed.x = maxSpeedX;
		}
        else if (this.speed.x < (-maxSpeedX))
		{
            this.speed.x = -maxSpeedX;
		}
        if (this.speed.y > maxSpeedY)
		{
            this.speed.y = maxSpeedY;
		}
        else if (this.speed.y < (-maxSpeedY))
		{
            this.speed.y = -maxSpeedY;
		}
    }
}
