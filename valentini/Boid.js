class Boid
{
	constructor(canvas, ctx, angle, vx, vy)
	{	
		this.ctx = ctx;
		this.x = 100;
		this.y = 100;
		this.vx = vx;
		this.vy = vy;			
		this.color = this.randomColor();	
		this.angle = angle;
		this.radians = Math.PI/180;
		this.canvas = canvas;		
	} 
		  
	draw() {		
		this.ctx.beginPath();
		this.ctx.moveTo(0, 0);
		this.ctx.lineTo(0, 30);
		this.ctx.lineTo(45, 15);
		this.ctx.closePath();
		this.ctx.fillStyle = this.color;
		this.ctx.fill();
	}
	
	randomColor() {
		let colore;
		colore = Math.random() * (999999 - 0) + 0;		
		let col = "#" + parseInt(colore);
		console.log("Colore della forma: " + col);
		return col;
	}
	
	baricentro(){
		console.log("bx = " + (this.x + this.x + 30 + this.x + 15) / 3);
		var bx = (this.x + this.x + 30 + this.x + 15) / 3;
		console.log("by = " + (this.y + this.y + this.y - 45) / 3);		
		var by = (this.y + this.y + this.y - 45) / 3;		
	}	
	
	muovi() {		
	  this.angle = Math.atan2(this.vy, this.vx);
	  //console.log("Angolo rilevato" + angle);  
	  // save the current co-ordinate system 
	  // before we screw with it
	  Number(this.ctx.save());
	  //console.log("Coordinate salvate"); 
	  //console.log(Number(this.ctx.save())); 	  
	  this.ctx.clearRect(0,0, this.canvas.width, this.canvas.height);
	  // move to the middle of where we want to draw our image
	  this.ctx.translate(this.x, this.y);
	  // rotate around that point, converting our 
	  // angle from degrees to radians 
	  this.ctx.rotate(this.angle);
	  // draw it up and to the left by half the width
	  // and height of the image 
	  //this.draw(this, -(this.width/2), -(this.height/2));
	  this.draw();					//la ridisegno
	  this.x += this.vx;
	  this.y += this.vy;
	  this.ctx.restore();
	  //controllo se la figura è arrivata ai limiti del canvas
	  if (this.y + this.vy > this.canvas.height) {
		console.log("margine inferiore");
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.y = 0;
		this.baricentro();
	  }
	  if (this.x + this.vx > this.canvas.width) {
		console.log("margine destro");
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.x = 0;			  
		this.baricentro();
	  }
	  if (this.y + this.vy < 0) {
		console.log("margine superiore");		
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.y = this.canvas.height;	 			
		this.baricentro();
	  }
	  if (this.x + this.vx < 0) {
		console.log("margine sinistro");		
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.x = this.canvas.width;						
		this.baricentro();
	  }
		  
	  // and restore the co-ords to how they were when we began
	   
	}	
}

