const body = document.body
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

var piccione;
piccione = new Boid(canvas, ctx, 5, 5);

function velocita_x(){
	console.log("Velocità x rilevata = " + document.getElementById("range_x").value); //scrivo a schermo la velocità rilevata
	piccione.vx = Number(document.getElementById("range_x").value);
}

function velocita_y(){
	console.log("Velocità y rilevata = " + document.getElementById("range_y").value); //scrivo a schermo la velocità rilevata
	piccione.vy = Number(document.getElementById("range_y").value);
}


//piccione.draw();

function disegna(){
	piccione.draw();
	piccione.muovi();
	raf = window.requestAnimationFrame(disegna);	//richiamo la funzione stessa all infinito  (finché puntatore all interno del canvas)
}	