// Serve a VSCode
/** @type {CanvasRenderingContext2D} */
var ctx;
var canvas;
var accelerazioneGlobale = new Vettore(0, 0);
/**@type {Array.<Boid>} */
var boids = [];
var animationID;
var cronologiaPosizioni = [];
var scieAttive = false;
// Statistiche
var tempo = performance.now();
var tempoPrecedente = performance.now();

// Costanti
const nScia = 50;

//Temporaneamente qui
var raggioRicerca = 50;

function GetCanvas() {
    canvas = document.getElementById('canvas');

    if (canvas.getContext) {
        ctx = canvas.getContext('2d');
    }

    canvas.width = window.innerWidth - 50;
    canvas.height = window.innerHeight - 300;

    document.getElementById("speedValueX").innerText = "0";
    document.getElementById("speedValueY").innerText = "0";

    // Invece di fermare e riniziare l'animazione ogni volta che aggiungo un boid,
    // inizio adesso ad animare anche se non ci sono boid disegnati
    animationID = window.requestAnimationFrame(Animazione);

    // Handler per il resize
    document.body.onresize = function(){
        canvas.width = window.innerWidth - 50;
        canvas.height = window.innerHeight - 300;
    }
    
}

function AggiungiBoids(){
    let nBoids = parseInt(document.getElementById("nBoids").value);
    for (let i = 0; i < nBoids; i++){
        CreaBoid(rng(1, canvas.width), rng(1, canvas.height), canvas, ctx);
    }
}

function CreaBoid(x, y, canvas, ctx) {
    boids.push(new Boid(x, y, canvas, ctx));
}

function Animazione() {
    tempo = performance.now();
    // Dal frameTime calcolo il frameRate
    let fps = Math.floor(1 / (tempo - tempoPrecedente) * 1000)
    // Stampo il frame rate
    document.getElementById("fps").innerHTML = "FPS: " + fps;
    //console.log(tempo - tempoPrecedente)
    tempoPrecedente = tempo;
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // ForEach per ogni boid
    boids.forEach(element => {
        // Prima disegno la scia e dopo il boid, in modo che il boid sia al di sopra della scia
        // Gli disegno la scia se è attiva
        if (scieAttive)
            element.DisegnaScia();
        // Poi disegno e sposto il boid
        element.Sposta(accelerazioneGlobale);
    })
    // Salvo il tempo attuale
    tempo = performance.now();
    animationID = window.requestAnimationFrame(Animazione);
}

function UpdateSliderX() {
    let slider = document.getElementById("speedX");
    let sliderString = document.getElementById("speedValueX");
    accelerazioneGlobale.x = slider.value;
    sliderString.innerText = accelerazioneGlobale.x;
}

function UpdateSliderY() {
    let slider = document.getElementById("speedY");
    let sliderString = document.getElementById("speedValueY");
    accelerazioneGlobale.y = slider.value;
    sliderString.innerText = accelerazioneGlobale.y;
}

function rng(min, max) {
    return Math.floor((Math.random() * (max - min + 1)) + min);
}

function ToggleScie(){
    scieAttive = document.getElementById("scieAttive").checked;
}