class Boid {
    static larghezza = 7;
    static lunghezza = 17;
    disegnaRaggio = false;
    colore = "gray"
    cronologiaPosizioni = [];
    constructor(x, y, canvas, ctx) {
        this.posizione = new Vettore(x, y);
        this.velocita = new Vettore(0, 0);
        do {
            this.accelerazione = new Vettore(rng(-1, +1), rng(-1, +1));
        } while (this.accelerazione.x == 0 || this.accelerazione.y == 0);


        this.canvas = canvas;
        /**@type {CanvasRenderingContext2D} */
        this.ctx = ctx;
    }

    Draw() {
        this.ctx.fillStyle = this.colore;
        this.ctx.beginPath();


        this.ctx.moveTo(this.posizione.x, this.posizione.y);
        this.ctx.lineTo(this.posizione.x + (-Boid.lunghezza), this.posizione.y + (-Boid.larghezza));
        this.ctx.lineTo(this.posizione.x + (-Boid.lunghezza), this.posizione.y + Boid.larghezza);
        this.ctx.fill();

        // disegno il raggio di ricerca
        if(this.disegnaRaggio){
            this.ctx.arc(this.x, this.y, raggioRicerca, 0, Math.PI*2);
            this.ctx.stroke;
        }
            
    }

    Sposta(accelerazioneGlobale) {

        // Prima calcolo la velocità
        this.velocita.x = this.accelerazione.x * accelerazioneGlobale.x;
        this.velocita.y = this.accelerazione.y * accelerazioneGlobale.y;

        // Calcolo l'angolo
        let angolo = this.velocita.CalcolaAngolo();

        // Salvo la posizione iniziale
        this.ctx.save();
        // Mi sposto all'inizio del triangolo
        this.ctx.translate(this.posizione.x, this.posizione.y);
        // Ruoto il canvas
        this.ctx.rotate(angolo);
        //Linea dell'angolo
        //this.ctx.fillStyle = "red"
        //this.ctx.fillRect(0, 0, 30, 1);
        // Sposto il canvas all'origine
        this.ctx.translate(-this.posizione.x, -this.posizione.y);
        // Disegno il triangolo
        this.Draw();
        // Resetto la rotazione
        this.ctx.restore();

        // Prima di spostare il boid salvo l'ultima posizione nell'array
        let tmp;
        this.cronologiaPosizioni.unshift(tmp = {
            x: this.posizione.x,
            y: this.posizione.y,
        });
        // Trim a massimo nScia posizioni
        this.cronologiaPosizioni = this.cronologiaPosizioni.slice(0, nScia);

        // Sposto il boid
        this.posizione.x += this.velocita.x;
        this.posizione.y += this.velocita.y;

        // Bordo basso
        if (this.posizione.y > this.canvas.height) {
            this.posizione.y = 0;
        }

        // Bordo destro
        if (this.posizione.x > this.canvas.width) {
            this.posizione.x = 0;
        }

        // Bordo sinistro
        if (this.posizione.x < 0) {
            this.posizione.x = this.canvas.width;
        }

        // Bordo alto
        if (this.posizione.y < 0) {
            this.posizione.y = this.canvas.height;
        }
    }

    DisegnaScia() {
        // Variabile per segnare l'ulitma volta che il disegno è stato fermato
        let ultimoDisegno = 0;
        // Disegno la scia
        for (let i = 0; i < this.cronologiaPosizioni.length; i++) {
            // Per evitare che la righa salti in giro, controllo se la prossima posizione è a più di un terzo della grandezza del canvas
            // Calcolo la distanza
            let distanza;
            try {
                distanza = Math.sqrt((this.cronologiaPosizioni[i + 1].x - this.cronologiaPosizioni[i].x) ** 2 +
                    (this.cronologiaPosizioni[i + 1].y - this.cronologiaPosizioni[i].y) ** 2);
            } catch {
                distanza = 0;
            }

            // controllo se questa è o la prima volta che deve essere iniziato il disegno, oppure un'altra dopo che il disegno è stato ultimato
            // ma è ancora incompleto
            if (i == ultimoDisegno) {
                // Anche qui controllo e salto il disegno se la distanza è troppa
                if (distanza >= canvas.width / 3 || distanza >= canvas.height / 3) {
                    ultimoDisegno = i + 1;
                } else {
                    ctx.beginPath();
                    ctx.moveTo(this.cronologiaPosizioni[i].x, this.cronologiaPosizioni[i].y);
                }
            } else {

                // Controllo se la distanza è maggiore di un terzo del canvas
                if (distanza >= canvas.width / 3 || distanza >= canvas.height / 3) {
                    // Stroke e rinizio settando ultimaposizione a i + 1
                    ctx.stroke();
                    ultimoDisegno = i + 1;
                } else {
                    ctx.lineTo(this.cronologiaPosizioni[i].x, this.cronologiaPosizioni[i].y);
                }
            }
        }

        ctx.strokeStyle = "green";
        ctx.stroke();
    }
}
