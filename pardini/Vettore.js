class Vettore{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }

    CalcolaAngolo(){
        return Math.atan2(this.y, this.x);
    }
}