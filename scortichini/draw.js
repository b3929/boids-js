
var canvas = document.getElementById('canvas')
var ctx = canvas.getContext('2d')
var vx = 0;
var vy = 0;
var angle = 0;
var boids = [];

/* boids.Disegna();
boids.Muovi();*/
function Rotazione()
{
	this.ctx.clearRect(0,0,canvas.width,canvas.height);

    //boids.Muovi();
    for(let i =0;i<boids.length;i++)
    {
        boids[i].Muovi();
    }

    window.requestAnimationFrame(Rotazione);

}
function Aggiungi()
{
	Rotazione();
	boids.push(new Boid(Math.random() * 1000,Math.random() * 500,ctx,canvas,vx,vy,angle));	
}
function VX()
{
    for(let i =0;i<boids.length;i++)
    {
        boids[i].vx = parseInt(document.getElementById('rangex').value);
    }

}
function VY()
{
    for(let i =0;i<boids.length;i++)
    {
        boids[i].vy = parseInt(document.getElementById('rangey').value);
    }
    //boids.vy = parseInt(document.getElementById('rangey').value);
}


//window.requestAnimationFrame(A);
