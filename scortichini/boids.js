class Boid{

	constructor(x,y,ctx,canvas,vx,vy,angle){
		this.angle = angle;
		this.x=x;
		this.y=y;
		this.ctx=ctx;
		this.canvas=canvas;
		this.vx = 5;
		this.vy = 5;
		this.ctx.fillStyle = 'green';
	}

	

	Disegna()
	{
		//this.ctx.clearRect(0,0,canvas.width,canvas.height);
		this.ctx.beginPath();
		this.ctx.moveTo(0,0);
		this.ctx.lineTo(50,50);
		this.ctx.lineTo(20,50);
		this.ctx.lineTo(20,75);
		//this.ctx.closePath();
		this.ctx.fill();
		
	}

	Muovi()
	{	
		this.angle=Math.atan2(this.vy,this.vx);
		this.ctx.save();
		this.ctx.translate(this.x,this.y);
		this.ctx.rotate(this.angle);
		//this.ctx.translate(-this.x,-this.y);
		this.Disegna();
		this.ctx.restore();
		this.y += this.vy;
		this.x += this.vx;
		if (this.x > this.canvas.width ||
            this.x < 0) 
		{
			this.vx = -this.vx;
        }
        if (this.y > this.canvas.height ||
            this.y < 0) 
		{
			this.vy = -this.vy;
        }

	}



}