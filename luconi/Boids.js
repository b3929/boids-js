class Boid
{
	constructor(canvas, ctx, angle, vx, vy)
	{	
		this.ctx = ctx;
		this.x = 100;
		this.y = 100;
		this.vx = vx;
		this.vy = vy;			
		this.color = this.randomColor();	
		this.angle = angle;
		this.radians = Math.PI/180;
		this.canvas = canvas;		
	} 
		  
	draw() {		
		this.ctx.beginPath();
		this.ctx.moveTo(0, 0);
		this.ctx.lineTo(0, 30);
		this.ctx.lineTo(45, 15);
		this.ctx.closePath();
		this.ctx.fillStyle = this.color;
		this.ctx.fill();
	}
	
	randomColor() {
		let colore;
		colore = Math.random() * (999999 - 0) + 0;		
		let col = "#" + parseInt(colore);
		console.log("Colore della forma: " + col);
		return col;
	}
	
	baricentro(){
		//console.log("bx = " + (this.x + this.x + 30 + this.x + 15) / 3);
		var bx = (this.x + this.x + 30 + this.x + 15) / 3;
		//console.log("by = " + (this.y + this.y + this.y - 45) / 3);		
		var by = (this.y + this.y + this.y - 45) / 3;		
	}	
	
	muovi() {		
	  this.angle = Math.atan2(this.vy, this.vx);
	  Number(this.ctx.save());
	  this.ctx.clearRect(0,0, this.canvas.width, this.canvas.height);
	  this.ctx.translate(this.x, this.y);
	  this.ctx.rotate(this.angle);
	  this.draw();					//la ridisegno
	  this.x += this.vx;
	  this.y += this.vy;
	  this.ctx.restore();
	  if (this.y + this.vy > this.canvas.height) {
		console.log("margine inferiore");
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.y = 0;
		this.baricentro();
	  }
	  if (this.x + this.vx > this.canvas.width) {
		console.log("margine destro");
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.x = 0;			  
		this.baricentro();
	  }
	  if (this.y + this.vy < 0) {
		console.log("margine superiore");		
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.y = this.canvas.height;	 			
		this.baricentro();
	  }
	  if (this.x + this.vx < 0) {
		console.log("margine sinistro");		
		console.log("x: " + this.x);
		console.log("y: " + this.y);
		this.baricentro();
		this.x = this.canvas.width;						
		this.baricentro();
	  }
		  
	   
	}	
}
