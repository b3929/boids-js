
    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
	var width = canvas.width;
	var height = canvas.height;
	var dx=4;
	var dy=4;
	var d = 20;
	
	var x = d+dx+1;
    var y = d+dy+1;
    canvas.addEventListener("click", onClick, false);
	
    var raf;

	var velocitàX = -5
	var velocitàY = 5
	
	//Array di Boids
	var Boids = []
	
	//Array di colori
	const Colori = ['#7fff00', '#ff8c00', '#00bfff', 
				 '#ffd700', '#adff2f', '#ff69b4', 
				 '#ff4500', '#db7093', '#ffa500', 
				 '#ff0000', '#cd853f', '#b22222']
	
	DrawBoid()
	
	function DrawBoid()
	{	
		ctx.clearRect(0,0,canvas.width, canvas.height) //Ripulisco la schermata
		
		for (let i=0; i < Boids.length; i++)
		{
			Boids[i].movement(); 
		}
		raf = window.requestAnimationFrame(DrawBoid) //Ricorsione
	}
	
	//Funzione per generare i Boids al click del mouse sul canvas
	function onClick(e) {
    var element = canvas;
    var offsetX = 0, offsetY = 0

    if (element.offsetParent) 
	{
		do 
		{
			offsetX += element.offsetLeft;
			offsetY += element.offsetTop;
		}while ((element = element.offsetParent));
    }
	x = e.pageX - offsetX;
	y = e.pageY - offsetY;
	
	//Creazione Boids con colori e posizioni generate casualmente
	Boids.push(new Boid(x, y, velocitàX, velocitàY, Colori[Casuale(0,Colori.length)],ctx));
}
	
	function Clear()
	{
		ctx.clearRect(0,0,canvas.width, canvas.height) //Ripulisco la schermata
		Boids = [] //Ricreo l'array
	} 
	
	function ModificaVelocitàX()
	{	
		var sliderX = document.getElementById("velocitàX");
		var valoreX = document.getElementById("ValoreVelocitàX");
		velocitàX = parseInt(sliderX.value);
		valoreX.innerText = velocitàX;
		
		for (let i=0; i < Boids.length; i++)
		{
			Boids[i].vx = velocitàX; 
		}
	}
	
	function ModificaVelocitàY()
	{	
		var sliderY = document.getElementById("velocitàY");
		var valoreY = document.getElementById("ValoreVelocitàY");
		velocitàY = parseInt(sliderY.value);
		valoreY.innerText = velocitàY;
		
		for (let i=0; i < Boids.length; i++)
		{
			Boids[i].vy = velocitàY; 
		}
	}
	
	//Genera numeri casuali
	function Casuale(min, max)
	{
		return Math.floor((Math.random()* max ) + min);
	}
	
