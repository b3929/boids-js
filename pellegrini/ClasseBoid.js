class Boid
{
	constructor(x, y, vx, vy, colore, ctx)
	{
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		this.colore = colore;
		this.ctx = ctx;
	}
	
	draw()
	{
		this.ctx.beginPath();
		this.ctx.moveTo(0, 0);
		this.ctx.lineTo(-30, 10);
		this.ctx.lineTo(-30,-10);
		//this.ctx.closePath();
		this.ctx.fillStyle = this.colore;
		this.ctx.fill();
	}
	
	movement()
	{
		//---------------------------------------------------- Rotazione
		this.angolo = Math.atan2(this.vy, this.vx);
		this.ctx.save(); //Salva il contesto
		this.ctx.translate(this.x, this.y);
		this.ctx.rotate(this.angolo);
		//----------------------------------------------------
		
		this.draw();
		this.ctx.restore();
        this.x += this.vx;
        this.y += this.vy;

		//Verifico se la figura tocca il bordo del canvas
        if (this.y + this.vy > canvas.height || this.y + this.vy < 0) 
		{
          this.vy = -this.vy;
        }
        if (this.x + this.vx > canvas.width || this.x + this.vx < 0) 
		{
          this.vx = -this.vx;
        }
	}	
}