//Serve a VSCode
/** @type {CanvasRenderingContext2D} */

//TODOS
//X. Uniformare lingua (nomi var e metodi in eng)
//2. Ripristinare disegno aeroplanino ma con riferimento al centro della forma
//X. Refactoring codice (togliere parti inutili e rendere più leggibile)
//X  Migliorare layout pagina html slider (tabella 3x2 con gli slider dentro)
//X 5. Aggiungere label con numero boids correnti
//6. Boids colorati
//X  Pulsante reset
//8. Ottimizzazioni varie ed eventuali
//9. Personalizzare README
var ctx;
var canvas;
/**@type {Array.<Boid>} */
var boids = [];
var animationID;
//var cronologiaPosizioni = []; Da cancellare
var trailsOn = false;

//Statistiche
var time = performance.now();
var previousTime = performance.now();

var radius;
var randFactor;
var speedup = 2;

var cohesionFactor;
var separationFactor;
var alignmentFactor;

//Costanti
const nTrail = 50; //Scia

function GetCanvas() 
{
    canvas = document.getElementById('canvas');

    if(canvas.getContext) 
	{
        ctx = canvas.getContext('2d');
    }

    canvas.width = window.innerWidth - 50;
    canvas.height = window.innerHeight - 200;

    document.getElementById("cohesionValue").innerText = "0";
    document.getElementById("separationValue").innerText = "0";
	document.getElementById("alignmentValue").innerText = "0";
	document.getElementById("vrValue").innerText = "0";

    //Invece di fermare e riniziare l'animazione ogni volta che aggiungo un boid,
    //Inizio adesso ad animare anche se non ci sono boid disegnati
    animationID = window.requestAnimationFrame(Animation);

    //Handler per il resize
    document.body.onresize = function()
	{
        canvas.width = window.innerWidth - 50;
        canvas.height = window.innerHeight - 200;
    }
    
    //Handler per il click del canvas per l'aggiunta di un boid
    canvas.addEventListener("click", function(event)
	{
        CreateBoid(event.offsetX, event.offsetY, canvas, ctx);
    })
}

function AddBoids()
{
    let nBoids = parseInt(document.getElementById("nBoids").value);
    for (let i = 0; i < nBoids; i++)
	{
        CreateBoid(rng(1, canvas.width), rng(1, canvas.height), canvas, ctx);
    }
}

function CreateBoid(x, y, canvas, ctx) 
{
    boids.push(new Boid(x, y, canvas, ctx));
}

function Animation() 
{
	// Calcolo degli FPS
	time = performance.now();
    let fps = Math.floor(1 / (time - previousTime) * 1000) // Dal frameTime calcolo il frameRate
    document.getElementById("fps").innerHTML = "FPS: " + fps; // Stampo il frame rate
    previousTime = time;

	//---------------------------------------------------------------------------------------------------------------
	radius = parseInt(document.getElementById("vr").value);
	randFactor = (parseInt(document.getElementById("rf").value))/100;;
	speedup = parseInt(document.getElementById("sp").value);

	cohesionFactor = (parseInt(document.getElementById("cohesion").value))/10000;
	separationFactor = (parseInt(document.getElementById("separation").value))/10000;
	alignmentFactor = (parseInt(document.getElementById("alignment").value))/10000;
	
	
	document.getElementById("cb").innerText = "nboids: " + boids.length; //Conteggio corrente numero boids 
	document.getElementById("cohesionValue").innerText = cohesionFactor; //Stampa a video del valore della coesione
	document.getElementById("separationValue").innerText = separationFactor; //Stampa a video del valore della separazione
	document.getElementById("alignmentValue").innerText = alignmentFactor; //Stampa a video del valore dell'allineamento
	document.getElementById("vrValue").innerText = radius; //Stampa a video del valore del campo visivo
	document.getElementById("rfValue").innerText = randFactor; 
	document.getElementById("spValue").innerText = speedup;

	//---------------------------------------------------------------------------------------------------------------
    ctx.clearRect(0, 0, canvas.width, canvas.height);

	for(let i = 0; i < boids.length; i++)
	{
		if (trailsOn)
		{
            boids[i].DrawTrail();
		}
		boids[i].Move();
		CheckAround(i) // per ogni boid richiama la funzione che ne controlla l'intorno
	}
    //Salvo il tempo attuale
    time = performance.now();
	//ScrollBoids();
    animationID = window.requestAnimationFrame(Animation);
}

function Cohesion(subject, other)
{
	s = subject.position.clone();
	o = other.position.clone();
	o.subtract(s);
	
	return o;
}

function Separation(subject, other)
{
	s = subject.position.clone();
	o = other.position.clone();
	s.subtract(o);
	
	return s;
}

function Alignment(subject,other)
{
	o = other.speed.clone();
	return o;
}

function rng(min, max) 
{
    return Math.floor((Math.random() * (max - min + 1)) + min);
}

function ToggleTrails()
{
    trailsOn = document.getElementById("trailsOn").checked;
}

function CheckAround(index) // funzione che controlla intorno ad ogni boid
{
	let distance; //Distanza tra il boid corrente e quello che si sta verificando
	let boidsNumber = 0; //Numero di boids presenti nell'intorno
	let mediumAngle = 0; //Angolo medio dei boids presenti nell'intorno
	let color;// = '#' + Math.floor(Math.random()*16777215).toString(16);
	let boid = boids[index];
	let maxSpeedX = 100;
	let maxSpeedY = 100;		

	let fc = new Victor(0,0);
	let fs = new Victor(0,0);
	let fa = new Victor(0,0);
	let fr = new Victor( ( Math.random() - 0.5 ) * randFactor, ( Math.random() - 0.5 ) * randFactor);

	for(let i=0; i<boids.length; i++) // scorro tutto l'array dei boids
	{
		if(i!=index) //Tranne il boid corrente
		{
			distance = boids[index].position.distance(boids[i].position);
			if(distance<=radius) // se la distanza calcolata è inferiore al raggio di visione dei boids
			{
				mediumAngle+=boids[i].angle; //sommo l'angolo del boid alla somma di tutti gli angoli dei boid presenti nell'intorno
				boidsNumber++; //incremento il numero di boids

				fc.add(Cohesion(boid, boids[i]));
				fs.add(Separation(boid, boids[i]));
				fa.add(Alignment(boid, boids[i]));
			}	
		}
	}
	
	if(boidsNumber>0) //Se c'è almeno un altro boid nell'intorno
	{
		
		mediumAngle = mediumAngle/boidsNumber; // calcolo la media degli angoli
		
		fc.multiplyScalar(cohesionFactor/boidsNumber);
		fs.multiplyScalar(separationFactor/boidsNumber);
		fa.multiplyScalar(alignmentFactor/boidsNumber);
	}
	
	boids[index].ApplyForce(fc, maxSpeedX, maxSpeedY);
	boids[index].ApplyForce(fs, maxSpeedX, maxSpeedY);
	boids[index].ApplyForce(fa, maxSpeedX, maxSpeedY);
	boids[index].ApplyForce(fr, maxSpeedX, maxSpeedY);
	boids[index].speed.normalize();
	boids[index].speed.multiplyScalar(speedup);

}

function ScrollBoids() //Funzione che scorre tutto l'array dei boids 
{
	for(let i=0; i<boids.length; i++)
	{
		CheckAround(i) // per ogni boid richiama la funzione che ne controlla l'intorno
	}
}

function ResetCanvas()
{
	ctx.clearRect(0,0,canvas.width, canvas.height) //Ripulisco la schermata
	boids = []; //Ricreo l'array
} 