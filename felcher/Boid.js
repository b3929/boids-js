class Boid{
	constructor(dim, lato, x, y, vx, vy, color,ctx){
		this.dim=dim;
		this.lato=lato;
		this.x=x;
		this.y=y;
		this.vx=vx;
		this.vy=vy;
		this.color=color;
		this.ctx=ctx;
	}
	
	draw(){
		ctx.beginPath();
		ctx.fillStyle = this.color;
		ctx.moveTo(this.x, this.y); // sposto il cursore nel punto in cui inizia il primo lato
		ctx.lineTo(this.x + dim*2, this.y); // disegno il lato orizzontale
		ctx.lineTo(this.x, this.y + dim); // disegno il lato verticale
		ctx.moveTo(this.x + dim*2, this.y); // sposto il cursore su uno dei vertici creati
		ctx.lineTo(this.x, this.y + dim); // disegno l'ipotenusa
		ctx.closePath();
		ctx.fill();
	}
	
	move(){
		ctx.clearRect(0,0, canvas.width, canvas.height);
		this.draw();
		
		
		this.x += this.vx;
		this.y += this.vy;

		if (this.y  > canvas.height - lato || this.y + this.vy < 0) {
			this.vy = -this.vy ;
		}
		if (this.x + this.vx > canvas.width - lato || this.x + this.vx < 0) {
			this.vx = -this.vx;
		}

		raf = window.requestAnimationFrame(draw);
	}
}
//Boid triangle=new Boid(20, 20, 5, 10, 2, 2, 'orange');


/*function corner () 
{
	r=Math.sqrt(vx*2+vy*2);
	setTimeout((200)=> {angolo= Math.asin (vx/r)});
	return angolo;
}*/