let canvas = document.getElementById('canvas'); // oggetto canvas
let boids = new Array(); // array di boids
let radius = 70; // raggio di visione di ogni boid
	
function MoveBoids(boid,i) // funzione che muove i boids
{
	
	boid.speed.x = parseInt(document.getElementById("speedX").value); // lettura della velocità orizzontale
	boid.speed.y = parseInt(document.getElementById("speedY").value); // lettura della velocità verticale
	document.getElementById("speedXValue").innerText = "Value: " + boid.speed.x; // stampa a video del valore della velocità orizzontale
	document.getElementById("speedYValue").innerText = "Value: " + boid.speed.y; // stampa a video del valore della velocità verticale
	document.getElementById("angle").innerHTML = "Angle: " + Math.floor(boid.angle * (180/Math.PI)) + "&deg"; // stampa a video dell'anglo espresso in gradi

	boid.draw(); // disegno del boid
	
	boid.position.x = boid.position.x + boid.speed.x; // incremento posizione orizzontale
	boid.position.y = boid.position.y + boid.speed.y ; // incremento posizione verticale

	if (boid.position.y + boid.speed.y - boid.height > canvas.height) //spostamento boid dal basso all'alto del canvas
		boid.position.y = 0;
	
	if(boid.position.y + boid.speed.y  + boid.height < 0) //spostamento boid dall'alto al basso del canvas
		boid.position.y = canvas.height;

	if (boid.position.x + boid.speed.x - boid.height> canvas.width) //spostamento boid da destra a sinistra del canvas
		boid.position.x = 0;
	
	if(boid.position.x + boid.speed.x  + boid.height < 0) //spostamento boid da sinistra a destra del canvas
		boid.position.x = canvas.width;
	
	if(i==boids.length-1) // se il boid che si sta muovendo è l'ultimo
		raf = window.requestAnimationFrame(SelectBoid); // ricomincio da capo
			
}

function SelectBoid() // funzione che seleziona i boid dall'array e li fa muovere
{
	canvas.getContext('2d').clearRect(0,0, canvas.width, canvas.height); // pulizia del canvas
	
	for(let i=0;i<boids.length;i++) 
	{
		MoveBoids(boids[i],i);
	}
}

function CreateBoids(boidsNumber) // funzione che aggiunge il numero di boid specificato nei parametri all'array
{
	for(let i=0;i<boidsNumber;i++)
	{
		boids.push(new Boid(new Vector(Math.floor(Math.random() * canvas.width),Math.floor(Math.random() * canvas.height)),new Vector(5,10),new Vector(10,10), canvas.getContext('2d'))); // aggiunta nell'array di un nuovo boid in una posizione casuale del canvas
	}
}

function CheckAround(boid, index) // funzione che controlla intorno ad ogni boid
{
	let distance; // distanza tra il boid corrente e quello che si sta verificando
	let averagePosition = new Vector(0,0); // posizione media dei boids nell'intorno
	let boidsNumber = 0; // numero di boids presenti nell'intorno
	let mediumAngle = 0; // angolo medio dei boids presenti nell'intorno
	let color;// = '#' + Math.floor(Math.random()*16777215).toString(16);

	for(let i=0; i<boids.length; i++) // scorro tutto l'array dei boids
	{
		if(i!=index) //tranne il boid corrente
		{
			with(Math) distance = sqrt(pow(boid.position.y-boids[i].position.y, 2)+pow(boid.position.x-boids[i].position.x, 2)); // calcolo la distanza tramite la formula  d = sqrt((y1-y2)^2 + (x1-x2)^2)
			if(distance<=radius) // se la distanza calcolata è inferiore al raggio di visione dei boids
			{
				//boids[i].color = '#009500';
				/*if(i>index)
					boids[i].color = boids[index].color;*/
					//boids[i].color = color;
				//console.log("boid " + index +" vicino a boid "+ i);
				averagePosition.x+=boids[i].position.x; //sommo la posizione del boid alla somma di tutte le altre posizioni dei boid presenti nell'intorno
				averagePosition.y+=boids[i].position.y;
				mediumAngle+=boids[i].angle; //sommo l'angolo del boid alla somma di tutti gli angoli dei boid presenti nell'intorno
				boidsNumber++; //incremento il numero di boids
			}
				
		}
	}

	

	if(boidsNumber>0) // se c'è almeno un altro boid nell'intorno
	{
		color = '#00' + Math.floor((170/3)*boidsNumber).toString(16) +'00'; // cambio tonalità del verde in base al numero di boid nell'intorno
		boid.color = color; // assegnazione del colore
		averagePosition.x = averagePosition.x/boidsNumber; // calcolo la media delle posizioni
		averagePosition.y = averagePosition.y/boidsNumber;
		mediumAngle = mediumAngle/boidsNumber; // calcolo la media degli angoli
	}
	
}

function ScrollBoids() // funzione che scorre tutto l'array dei boids 
{
	for(let i=0; i<boids.length; i++)
	{
		CheckAround(boids[i], i) // per ogni boid richiama la funzione che ne controlla l'intorno
	}
}

function AddBoid(e) // funzione che aggiunge un boid
{
	boids.push(new Boid(new Vector(e.offsetX, e.offsetY),new Vector(5,10),new Vector(10,10), canvas.getContext('2d'))); // viene aggiunto un boid nella posizione del click (che viene specificato dall'evento passato come parametro)
	ScrollBoids();
}

canvas.addEventListener('click', function(event){ 
	AddBoid(event); // se si clicca in un punto all'interno del canvas viene richiamata la funzione AddBoid
});

CreateBoids(30); //creazione di 30 boids
ScrollBoids();