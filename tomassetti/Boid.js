class Boid // classe Boid
{
	constructor(position, speed, acceleration, ctx)
	{
		this.position = position; // Vettore bidimensionale che indica la posizione del boid 
		this.speed = speed; // Vettore bidimensionale che indica la velocità del boid 
		this.acceleration = acceleration; // Vettore bidimensionale che indica l'accelerazione del boid 
		this.side= side; // lunghezza della base del boid
		this.height = height; // altezza del boid
		this.ctx = ctx; // contesto 2d del canvas
		this.angle=0; // angolo di rotazione del boid
		this.color = '#000000'; // assegnazione del colore nero
	}
	
	draw()
	{
		//calcolo dell'angolo di rotazione
		this.angle = Math.atan2(this.speed.y, this.speed.x);
		
		//salvataggio della situazione corrente
		this.ctx.save();
		this.ctx.translate(this.position.x, this.position.y);
		this.ctx.rotate(this.angle);
		
		// disegno del boid
		this.ctx.beginPath();
		this.ctx.moveTo(0, 0);
		this.ctx.lineTo(-30,-10);
		this.ctx.lineTo(-40,-25);
		this.ctx.lineTo(-50,-10);
		this.ctx.lineTo(-60,-5);
		this.ctx.lineTo(-60,5);
		this.ctx.lineTo(-50,10);
		this.ctx.lineTo(-40,25);
		this.ctx.lineTo(-30,10);
		/*
		this.ctx.lineTo(-this.height, -this.side/2);
		this.ctx.lineTo(-(this.height-10), 0);
		this.ctx.lineTo(-this.height, this.side/2);
		*/
		//this.ctx.lineTo(0, 0);
		this.ctx.fillStyle = this.color;
		this.ctx.fill();
		
		// ritorno alla situazione precedente
		this.ctx.restore();
	}
	
	cohesion()
	{
		
	}
	
	separation()
	{
		
	}
	
	alignment()
	{
		
	}
}